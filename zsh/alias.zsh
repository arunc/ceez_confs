ulimit -c 0

# use this to setup the monitor correctly.
# xrandr  --output DP2 --auto --right-of eDP1

setxkbmap de
setxkbmap -option ctrl:nocaps
alias caps-to-control='setxkbmap -option ctrl:nocaps'

mkgit () {
    mkdir -p ${1}
    
    touch  ${1}/.gitignore 
    echo "tmp*"            >>  ${1}/.gitignore 
    echo "tmp/**"          >>  ${1}/.gitignore 
    echo "latex.out"       >>  ${1}/.gitignore 
    echo "latex.out/**"    >>  ${1}/.gitignore 
    echo ".emacs.desktop*" >>  ${1}/.gitignore 
    echo "arun*" >>  ${1}/.gitignore 
}

# PDF modifier, rotate/extract
alias pdfgui=pdfmod
alias pdfsplit=pdfmod
alias pdfextract=pdfmod
alias pdfcompress='convert -density 200x200 -quality 60 -compress jpeg '

alias off='sudo poweroff'

# This one works far better than mousepad.
textToPdf ()
{
    if [ -z $1 ]; then printf "[i] Usage : textToPdf <file.txt>    [results in file.txt.pdf]\n";
    elif [ ! -e $1 ]; then printf "[e] File not found $1\n";
    else 
	    enscript -p  ${1}.ps  $1 
	    ps2pdf ${1}.ps ${1}.pdf && \rm -f ${1}.ps
    fi
}
alias txtToPdf=textToPdf

alias ev='open'
alias -s pdf='ev'
alias -s PDF='ev'
alias -s ps='ev'
alias -s PS='ev'


# Thunderbird functions.
alias th=' thunderbird -tray >& /dev/null &! '
# attach the file and start a new msg.
# function noted for future references. 
# http://askubuntu.com/questions/5431/how-can-i-email-an-attachment-from-the-command-line
# http://askubuntu.com/questions/48471/how-can-i-cat-a-file-as-an-attachment-on-a-new-thunderbird-email-message-from?rq=1
thunderbird-compose () {
    thunderbird -remote  "xfeDoCommand(composeMessage,attachment='$(readlink -f $1)')"
}

lp () { leafpad $1  &!; }
mp () { mousepad $1 &!; }

encrypt () {
    gpg2 -e -r chandrasekharan.arun@gmail.com $1 && \rm -f $1
}
decrypt () { gpg2 $1 && \rm -f $1 }


# $@     - always split words to array
# $*     - always split words to array and remove empty items.
# ${==*} - same as above without word-splitting
# ${=*}  - same as above with forced word-splitting 
# http://zsh.sourceforge.net/FAQ/zshfaq03.html
calculate () {
    echo $(( ${==*} ))
}
alias q='noglob calculate'

# make directory and cd to it.
md () {  mkdir -p $1 && cd $1; }
alias mk='mkdir -p '

ed ()  # Load the emacs-desktop-file if available.
{
    if [ -e ./.emacs.desktop ]; then emacs -desktopload . >& /dev/null &! ;
    else emacs $1  >& /dev/null &!
    fi
}

# bg gray20 will avoid the annoying white window startup and then applying the background theme
#alias emacs='/usr/bin/emacs -bg gray20'
alias emacs='/usr/bin/emacs -bg gray20 --geometry=135x60'
# fh is full height
#alias emacs='/usr/bin/emacs -bg gray20 -fh'
alias emacs-minimal='/usr/bin/emacs -Q -bg gray20 -fg white --geometry=100x50 -l ~/.emacs.d/minimal.el'
alias em=emacs-minimal

alias cal=pal
alias palr='pal -r 31'
alias rename='echo "use zmv -W \"*.lis\" \"*.txt\""'


# Quickly show binary and hex digits for a given decimal.
# Use the base program from the project : git@gitlab.com:arunc/base.git
#--base() {
#--    setopt LOCAL_OPTIONS C_BASES OCTAL_ZEROES
#--    printf "decimal %s       =       %08d       0x%x\n" $1 ${$(([#2] $1))#2\#} $(($1))
#--}

# Run latexrun and open in evince
lr() {
    STEM=`basename ${1} .tex`;
    latexrun ${STEM}.tex &&   ev ${STEM}.pdf;
}
    
# -x4 tabs, -X do not flush screen, -F fit in one screen if possible
alias le='less -x4 -X -F '
alias zless='zless -X -F '
alias mo=more

# If this is a serious business should read http://stackoverflow.com/questions/3041530/how-to-get-command-history-by-cursor-key-in-linux-tclsh-tcl 
alias tclsh='rlwrap tclsh'

# should have setopt autocd in zshrc
#alias -g ...='../..'
#alias -g ....='../../..'
#alias -g .....='../../../..'


# For deutsch and english keyboard and viceversa
# May need to do 'loadkeys de'
alias english='setxkbmap us'
alias deutsch='setxkbmap de'
alias eng='setxkbmap us'
alias deu='setxkbmap de'

# remove the above ones.
alias gts='git status '
alias gta='git add '
alias gtc='git commit '
alias gtl='git log --pretty=fuller'
alias git-revert='git stash; git stash drop'

alias hgl='hg log'
alias hgs='hg status'
alias hgc='hg commit'


alias javaChangeVersion='echo use update-java-alternatives -s name-of-jdk. And change /etc/profile.d/jdk.*'
alias changeJavaVersion='echo use update-java-alternatives -s name-of-jdk. And change /etc/profile.d/jdk.*'

alias chmod_make_proper='chmod +x *; chmod o-w *'

alias help=' run-help'
#alias chrome=' google-chrome-stable'
chrome ()
{
    if [ -e $1 ]; then google-chrome-stable $1 >& /dev/null &!;
    else google-chrome-stable >& /dev/null &!;
    fi
}
alias cr='google-chrome-stable >& /dev/null &!'
alias sdcv='rlwrap sdcv'
alias dict='rlwrap sdcv'
alias pdflatexcleanup='\rm -f *.aux *.log *.out'
alias oo=openoffice4
alias bc=zcalc

alias nt="nautilus .  >& /dev/null &!"
#alias nt=" nemo .  >& /dev/null &!"
#alias nt="thunar .  >& /dev/null &!"
#alias nt="caja . "
#alias disp=gpicview
##alias disp='eog'
#alias disp=eom
#alias ev=evince
alias imageToPdf=convert
alias pdf-To-Text=ocrmypdf
alias dotToPng="dot -Tpng"
alias dotToPs="dot -Tps" 
alias ls='ls --color=yes'
alias csvview='column -s, -t '
alias viewcsv='column -s, -t '
alias so=source
# ^space is needed to ignore this entry to history. else entire history fills up with c, l etc.. 
alias l=' ls -ltrh'
alias ö=' ls -ltrh'
alias p=' pwd'
alias la=' ls  -a'
alias c=' clear'
alias wh=' which'
alias h=' history'
alias pd=pushd
#alias d=' dirs -v'
alias d='cd '
alias dc='cd '
#alias dir='l | grep "^d"= '
# Grep, Sed always take the Extended Regex
alias grep='grep -E'
alias sed='sed -r'
alias ll=' ls -l -h --color=tty'
alias ks=' ls'
#alias less ='more \!* | \less'
# no need of string for less, just show prompt.
alias less='less -X -x4 -F -w -Ps '
alias cd2='cd ../../'
alias cd3='cd ../../../'
alias cd4='cd ../../../../'
alias cd5='cd ../../../../../'
# find is case insensitive
alias fin="find ./* -iname "
# find all c++ source and header files. to be used with xargs grep
alias fih="find ./* -iregex '.*\.hp*$' -type f "
alias fic="find ./* -iregex '.*\.cp*$' -type f "
alias vlc=' /usr/bin/vlc' 
alias fh='firefox --new-tab about:newtab >& /dev/null'
alias ff='firefox --new-tab about:newtab >& /dev/null'
# locate as case insensitive.
# -p skips spaces and special char in names, but this is slow.
alias locate='/usr/bin/locate -i '
alias loc='/usr/bin/locate -i '

# Pager settings with colored less.
# Colors
default=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
purple=$(tput setaf 5)
orange=$(tput setaf 9)

# Less colors for man pages
export PAGER=less
# Begin blinking
export LESS_TERMCAP_mb=$red
# Begin bold
export LESS_TERMCAP_md=$orange
# End mode
export LESS_TERMCAP_me=$default
# End standout-mode
export LESS_TERMCAP_se=$default
# Begin standout-mode - info box
export LESS_TERMCAP_so=$purple
# End underline
export LESS_TERMCAP_ue=$default
# Begin underline
export LESS_TERMCAP_us=$green

alias ch=chmod
alias chx='chmod +x '

alias cdc=cd
export PAGER='less -X'

alias open='xdg-open >& /dev/null'
alias o='xdg-open >& /dev/null'

tmux_ssh() {
    if [[ -z $1 ]]; then 
      echo "Usage: tn session-name  # creates/attaches to a new tmux SSH session in osscmp31"; 
    else
      tmux new-session -A -s "${1}" -d 'ssh -X arun@osscmp31' \; attach
    fi
}
#alias tn_ssh=tmux_ssh
#alias tn='tmux -2 new-session -s'
#alias t='tmux list-sessions'
#alias tm=tmux_ssh
#alias ta="tmux -2 attach-session -d -t"

alias t='emacs-minimal /home/arun/Documents/toDo.txt &!'
alias td='emacs-minimal /home/arun/MEGAsync/currentWork/onespin-todo.txt &!'

# alias ping='prettyping -nolegend '

alias aigtoaig=/projects/coding/aiger/aigtoaig
alias ee="clear;clear;clear;clear;clear;clear;clear;clear;clear;clear;clear;clear;clear;clear;clear;clear"
# no need of ghostscript
alias gs="echo what?"

alias gw="cd /projects/coding/satvik4/build"
alias gf="cd /projects/MegaSync/Financials"


alias up=uptime

alias abc=/projects/coding/abc/abc

vlogToBtor()
{
    usage="Usage: $0 design.v   // output will be design.btor";
    if [[ -z $1 ]]; then echo "No Input Verilog given"; echo $usage; return; fi;
    if [[ ! -f $1 ]]; then echo "Cannot find input Verilog"; echo $usage; return; fi;

    vlog=$1
    btor=$( basename $1  .v)
    btor="${btor}.btor"

    yosys -Q -q -T -p "read_verilog ${vlog}; proc; fsm; dffunmap; write_btor ${btor}";
}

git-commit()
{
    echo "export MyDate=\"Wed Feb 16 14:00 2011 +0100\" && LC_ALL=C GIT_COMMITTER_DATE=\"\${MyDate}\" GIT_AUTHOR_DATE=\"\${MyDate}\" git commit --date \"\${MyDate}\""
}
