muttrc should be in mutt/muttrc // no .muttrc
But offlineimaprc should be ~/.offlineimaprc // there is a "." preceding it

follow the instructions as in https://opensource.com/article/21/7/gmail-linux-terminal ::

sudo apt-get install mutt
sudo apt-get install offlineimaprc
Must set up 2FA for GMAIL (arun.cn.mail already done)
Enable IMAP in GMAIL (arun.cn.mail already done)

Also it does take about 2-3 hours to sync up fully for the first time, as the article mentioned.
