My experiments with VPN setup in Linux
Install networkmanager-openvpn-gnome
1) sudo apt-get install network-manager-gnome openvpn
2) Then first install ntp and make sure that the time is synced wrt. internet
3) Added group nobody and user nobody to the .ovpn to makesure that there is no permission issues
4) Removed /etc/NetworkManager and rebooted.
5) Removed ipv6
    sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
    sudo sysctl -w net.ipv6.conf.disable_ipv6=1
check if ipv6 is gone this is gone using ip a

6) changed auth-retry nointeract from auth-retry interact
7) Saw one error that link-mtu and tun-mtu are not correct with journalctl -f
Updated tun-mtu 1392
link-mtu 1492
in the .ovpn file
This solved the issue

Now, the next problem is while connected to VPN, the internet is not working

This is because we need to add Route for ipv4 section.
Open the OpenVpn connection in NetworkManager, in the IPV4 section tick the "Use this connection only for resources on this network"
For a graphical depiction see:  https://askubuntu.com/questions/655806/openvpn-connecting-but-no-internet-access-on-ubuntu-16-04-18-04



