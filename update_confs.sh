#!/usr/bin/env bash


mkdir -p emacs
mkdir -p bash
mkdir -p zsh
mkdir -p xfce
mkdir -p lxde
mkdir -p vim


cp ~/.emacs ./emacs/emacs
cp ~/.emacs_nw ./emacs/emacs_nw

cp ~/.vimrc ./vim/vimrc

cp ~/.bashrc ./bash/bashrc
cp ~/.alias.lst ./bash/alias.lst


cp ~/.zshrc ./zsh/zshrc
cp ~/.alias.zsh ./zsh/alias.zsh

if [ -e /usr/bin/xfce4-custom-startup.sh ]; then cp /usr/bin/xfce4-custom-startup.sh ./xfce/; fi

if [ -e /usr/bin/zoomin ];  then cp /usr/bin/zoomin  ./xfce/; fi
if [ -e /usr/bin/zoomout ]; then cp /usr/bin/zoomout ./xfce/; fi

if [ -e ~/.config/openbox/lubuntu-rc.xml ]; then cp ~/.config/openbox/lubuntu-rc.xml ./lxde/; fi

if [ -e /usr/bin/dpkg ]; then 
    dpkg --get-selections | grep -v deinstall > ./lxde/lubuntu-pkgs.txt
fi

currDate=`date -Iminutes`
\rm -f updated_on_*
touch updated_on_${currDate}
