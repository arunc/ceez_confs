1. For xfce-custom startup script and use the xfce4-terminal drop down feature.
    -> xfce4-session-settings
     -> Application Autostart
      -> Add an entry "xfce4-terminal"
         Command: xfce4-terminal --drop-down --hide-menubar -e '/usr/bin/xfce4-custom-startup.sh'

2. Map capslock to AltGr for german keyboards
    -> xfce4-session-settings
     -> Application Autostart
      -> Add an entry "Capslock mapper"
         Command: setxkbmap -option "lv3:caps_switch"
  
3. Map F12 to drop-down terminal
  -> xfce4-keyboard-settings
    -> Application Shortcuts
    -> Add new application: xfce4-terminal --drop-down
      and map it to F12
      The command is by default in toggling mode

4. Map F1, F2, F3 etc to xfce4-terminal tabs
  -> Enable shortcut accelerator modifications
     -> xfce4-settings-manager
       -> Appearance -> Settings -> Enable Editable Accelerators
     ( http://docs.xfce.org/xfce/xfce4-settings/appearance#menu_and_buttons )
  -> enable menu in xfce4-terminal
  -> Create as many tabs as needed
  -> Press F1 in the tabs section in the Menu
  -> Record these shortcuts

  http://unix.stackexchange.com/questions/166427/is-there-a-shortcut-missing-to-move-tab-in-xfce4-terminal-app
  Alternately for terminal settings copy the terminal directory to ~/.config/xfce4/terminal
     -> this directory has .scm which to take care of keyboard shortcuts directly

5. to get emacs bindings everywhere (for e.g., Ctrl-E to the end of the weblink in chrome, Ctrl-k to kill the line in mousepad)
Settings / Settings Editor / xsettings / Gtk / KeyThemeName
Click Edit property
Change value to Emacs

6. Stop bluetooth
sudo systemctl disable  bluetooth.service

7. Our keyboard shortcuts for XFCE
   Alt-space for program list and so on (mac style)
   Alt-H to hide window, Alt-W to close window etc
   Copy xfce4-keyboard-shortcuts.xml to ~/.config/xfce4/xfconf/xfce-perchannel-xml/


8. Proper touchpad settings:
   sudo cp 70-synaptics.conf /usr/share/X11/xorg.conf.d/70-synaptics.conf
   
   We still get the annoying the TapButton2 setting.
   This is overridden a second time in .alias.zsh
  
10. Remove the stupid mouse+alt/ctrl zoom everything setting
    From the Xfce Menu, Settings > Setting Editor. 
    In the Channel pane select ‘xfwm4’ and in the right pane scroll all the way down to ‘zoom_desktop’. 
    Uncheck the Value box.



