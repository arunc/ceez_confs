1. Use use-ocrmypdf-to-covert-pdf-to-text # ocrmypdf has very good results

1. To unmap the ctrl-alt-shift-up and ctrl-alt-shift-down from moving the current window
   to the next desktop. (These keys are used in emacs for copying above line)
   Issue the following in the terminal:
   
   $ gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-up "['']"
   $ gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-down "['']"
   $ gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "['<Control><Alt>Down']"
   $ gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up "['<Control><Alt>Up']"

   The last two are not really necessary. These are for switching the desktops.
   Effect should be immediate, without logout-login cycle.


2. For 3 workspaces default instead of dynamic workspaces, min-max buttons etc,
   Use gnome-tweak tool
   E.g., Gnome-tweak-> last entry "Workspaces"
          -> Workspace Creation: Static and Number of Workspaces: 3
   
   OR do:
   $ gsettings set org.gnome.mutter dynamic-workspaces false
   $ gsettings set org.gnome.desktop.wm.preferences num-workspaces 3
   $ gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'

3. Change the <Print> key settings

   $ gsettings set org.gnome.settings-daemon.plugins.media-keys window-screenshot-clip '<Ctrl><Alt>Print'
   $ gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot-clip '<Ctrl><Shift>Print'
   $ gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot '<Shift>Print'
   $ gsettings set org.gnome.settings-daemon.plugins.media-keys window-screenshot '<Alt>Print'

   OR
   change in keyboard settings
     gnome-control-center -> Keyboard
     -> Hit backspace to delete the setting.


4. Use Thunderbird instead of Evolution.
   -> gnome-control-center -> Details
      Change the defaults for Mail etc

   Currently don't know the gsettings of this one


5. Map capslock to AltGr for german keyboards
   Not putting any default, use "sm" (switch_monitor) in terminal


6. Map F12 to drop-down terminal

   gnome-control-center -> Keyboard
   -> Go to the end, custom-shortcut
              Add entry: drop-down terminal
       	      Command: xfce4-terminal --drop-down
	      Shortcut: F12
     
      The command is by default in toggling mode

7. ToDo.txt
   gnome-control-center -> Keyboard
   -> Go to the end, custom-shortcut
      	    Add entry: ToDo
	    Command:  /usr/bin/mousepad /home/rut/Documents/todo.txt
 	    Shortcut: Ctrl-Alt-T
	    
8. Map F1, F2, F3 etc to xfce4-terminal tabs
  -> Enable shortcut accelerator modifications
     -> xfce4-settings-manager
       -> Appearance -> Settings -> Enable Editable Accelerators
     ( http://docs.xfce.org/xfce/xfce4-settings/appearance#menu_and_buttons )
  -> enable menu in xfce4-terminal
  -> Create as many tabs as needed
  -> Press F1 in the tabs section in the Menu
  -> Record these shortcuts

  http://unix.stackexchange.com/questions/166427/is-there-a-shortcut-missing-to-move-tab-in-xfce4-terminal-app
  Alternately for terminal settings copy the terminal directory to ~/.config/xfce4/terminal
     -> this directory has .scm which to take care of keyboard shortcuts directly

9. Tint2
  -> mkdir -p ~/.config/tint2
    -> cp tint2rc  ~/.config/tint2/

10. Remove evolution-server 
    Evolution cannot be uninstalled. Gnome is a dependency. Will uninstall Gnome also.
    Rather remove this from startup
    -> cd /usr/share/dbus-1/services
    -> sudo ln -snf /dev/null  org.gnome.evolution.dataserver.AddressBook.service                 
    -> sudo ln -snf /dev/null  org.gnome.evolution.dataserver.Calendar.service
    -> sudo ln -snf /dev/null  org.gnome.evolution.dataserver.Sources.service
    -> sudo ln -snf /dev/null  org.gnome.evolution.dataserver.UserPrompter.service

   Nxt time these junks should not startup

