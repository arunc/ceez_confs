function mkgit ()
{
    mkdir -p ${1}
    
    touch  ${1}/.gitignore 
    echo "tmp*"            >>  ${1}/.gitignore 
    echo "tmp/**"          >>  ${1}/.gitignore 
    echo "latex.out"       >>  ${1}/.gitignore 
    echo "latex.out/**"    >>  ${1}/.gitignore 
    echo ".emacs.desktop*" >>  ${1}/.gitignore 
    echo "arun*" >>  ${1}/.gitignore 
}

function nameOfFile ()
{
    filename=$(basename "$1")
    extension="${filename##*.}"
    filename="${filename%.*}"
    echo $filename
    lower=$(echo "$filename" | tr '[:upper:]' '[:lower:]')
    echo "$lower"
}

# CD to the active finder window
cdToActiveFinderWindow ()
{
theFinderPath=`osascript <<END
    tell application "Finder"
    	try
           set thePath to get the POSIX path of (folder of the front window as string)
	on error
	   set thePath to get the POSIX path of (path to home folder as string)
	end try
    end tell
END
`
  cd "$theFinderPath";
}

alias ff=cdToActiveFinderWindow
alias cdf=cdToActiveFinderWindow

# Quicklook instead of open
function ql() { qlmanage -p $1 >& /dev/null &!; }

alias o=open
alias ev=open
alias nt="open ."
alias disp=open
alias lp=/Applications/TextEdit.app/Contents/MacOS/TextEdit
alias mp=/Applications/TextEdit.app/Contents/MacOS/TextEdit
alias te=/Applications/TextEdit.app/Contents/MacOS/TextEdit
alias mvim='open -a /Applications/MacVim.app/Contents/MacOS/MacVim'
alias gvim='open -a /Applications/MacVim.app/Contents/MacOS/MacVim'
alias vim='open -a /Applications/MacVim.app/Contents/MacOS/MacVim'
alias opera='open -a /Applications/Opera.app/Contents/MacOS/Opera'
alias firefox='open -a /Applications/Firefox.app/Contents/MacOS/firefox'
alias chrome='open -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome'
alias vlcApp=' /Applications/VLC.app/Contents/MacOS/VLC' 
alias sf=' /Users/arun/.bin/Safari.osascript'
alias sft=' /Users/arun/.bin/SafariTab.osascript'
alias sfp=' /Users/arun/.bin/SafariPrivate.osascript'

alias ch=chmod
alias chx='chmod +x '
alias chw='chmod +w '
alias ct=cat
alias off='sudo shutdown -h now'
alias df='df -P'
alias rename='echo "use zmv -W \"*.lis\" \"*.txt\""'
# -X: do not clear while exiting, -F no less if it fits in a screen
alias le='less -X -F -x4 -w -Ps'
alias mo=more
alias tclsh='rlwrap tclsh'  # SO-3041530
alias chmod_make_proper='chmod +x *; chmod o-w *'
alias pdflatexcleanup='\rm -f *.aux *.log *.out'
alias bc=zcalc
alias imageToPdf=convert
alias csvview='column -s, -t '
alias viewcsv='column -s, -t '
alias so=source
alias t="open /Users/arun/Documents/toDo.txt"

# make directory and cd to it.
function md () { mkdir -p $1 && cd $1; }
alias mk='mkdir -p '

alias -s pdf='ev'
alias -s PDF='ev'
alias -s ps='ev'
alias -s PS='ev'

# For deutsch and english keyboard and viceversa
# May need to do 'loadkeys de'
alias english='setxkbmap us'
alias deutsch='setxkbmap de'
alias eng='setxkbmap us'
alias deu='setxkbmap de'

# git options
alias gts='git status '
alias gta='git add '
alias gtc='git commit '
alias gtl='git log '

alias hgs='hg status && hg bookmarks && hg phase -r .'
alias hgl='hg log'
alias hgc='hg commit'
alias hga='hg add'

alias gk='cd /Users/arun/Kavitha'
alias gp='cd /Users/arun/Personal'
alias gw='cd /Users/arun/Work'
alias ga='cd /Users/arun/Work/Academic/projects'
alias gf='cd /Users/arun/Personal/Fiscal/Financials'
alias tf='le /Users/arun/Personal/Fiscal/Financials/BankAccounts.txt'
alias to='le /Users/arun/Personal/Personal/O2/O2_numbers.txt'


# -G is equivalent of GNU --color=yes
alias ls=' ls -G'
alias l=' ls -ltrh'
alias ö=' ls -ltrh'
alias p=' pwd'
alias la=' ls  -a'
alias c=' clear'
alias wh=' which'
alias hi=' history 100'
alias h=' history'
alias pd=pushd
alias d='cd '
alias dc='cd '
# Grep, Sed always take the Extended Regex
# grep ignore binary files, silent 
alias grep='grep -E -I -s'
alias sed='sed -r'
alias ll=' ls -l -h --color=tty'
alias ks=' ls'
#alias less ='more \!* | \less'
# no need of string for less, just show prompt.
alias less='less -w -Ps '
alias cd2='cd ../../'
alias cd3='cd ../../../'
alias cd4='cd ../../../../'
alias cd5='cd ../../../../../'
# find is case insensitive
alias fin="find ./* -iname "
# find all c++ source and header files. to be used with xargs grep
alias fih="find ./* -iregex '.*\.hp*$' -type f "
alias fic="find ./* -iregex '.*\.cp*$' -type f "
alias fihg="find ./* -iregex '.*\.hp*$' -type f | xargs grep "
alias ficg="find ./* -iregex '.*\.cp*$' -type f | xargs grep "


# spawn and disown, and dont display all warnings and errors
function vlc() { vlcApp $@ >& /dev/null &!; }
function ganttproject () { /usr/bin/ganttproject $@ >& /dev/null &! }
function gnumeric () { /usr/local/bin/gnumeric $@ >& /dev/null &! }
function acroread () { /usr/bin/acroread $1 >& /dev/null &! }
alias emacs26p1=/Applications/Emacs.app/Contents/MacOS/Emacs
alias Emacs=emacs26p1
alias emacs=emacs26p1
#function emacs()
#{
#    if [[ -z $1 ]]; then  emacs26p1 >& /dev/null &!;
#    else touch $1; emacs26p1 $1 >& /dev/null &!;
#    fi
#}
#alias emacs='/usr/bin/emacs -bg gray20 --geometry=100x50'
#alias emacs-minimal='/usr/bin/emacs -Q -bg gray20 -fg white --geometry=100x50 -l ~/.emacs.d/minimal.el'

alias enw='emacs26p1 --load ~/.emacs_nw  -nw '
# alias emacsclient="/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -a -n "

alias edd='emacs  --load ~/.emacs_server --daemon >& /dev/null &!'
alias emacsclient='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -c -n $1'
#alias e='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -c -n $1'
e() { emacs $1 >& /dev/null &!; }


function encrypt() { gpg2 -e -r chandrasekharan.arun@gmail.com $1 && \rm -f $1; }
function decrypt() { gpg2 $1 && \rm -f $1 }

# $@     - always split words to array
# $*     - always split words to array and remove empty items.
# ${==*} - same as above without word-splitting
# ${=*}  - same as above with forced word-splitting 
# http://zsh.sourceforge.net/FAQ/zshfaq03.html
function calculate ()
{
    echo $(( ${==*} ))
}
alias q='noglob calculate'


# Quickly show binary and hex digits for a given decimal.
# Use the base program from the project : git@gitlab.com:arunc/base.git
#--base() {
#--    setopt LOCAL_OPTIONS C_BASES OCTAL_ZEROES
#--    printf "decimal %s       =       %08d       0x%x\n" $1 ${$(([#2] $1))#2\#} $(($1))
#--}


# Pager settings with colored less.
# Colors
default=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
purple=$(tput setaf 5)
orange=$(tput setaf 9)

# Less colors for man pages
export PAGER=less
# Begin blinking
export LESS_TERMCAP_mb=$red
# Begin bold
export LESS_TERMCAP_md=$orange
# End mode
export LESS_TERMCAP_me=$default
# End standout-mode
export LESS_TERMCAP_se=$default
# Begin standout-mode - info box
export LESS_TERMCAP_so=$purple
# End underline
export LESS_TERMCAP_ue=$default
# Begin underline
export LESS_TERMCAP_us=$green


# # tmux settings
# alias tn='tmux new-session -A -s'
# alias tl='tmux list-sessions'
# alias t='tmux list-sessions'
# alias t0='tmux new-session -A -s 0'
# alias t1='tmux new-session -A -s 1'
# alias t2='tmux new-session -A -s 2'
# alias t3='tmux new-session -A -s 3'
# alias tf="tmux attach-session -t fin ||  tmux new-session -A -s fin -d 'less /Users/arun/personal/Fiscal/Financials/BankAccounts.txt' \; split-window -h -d \; attach"
# alias to="tmux attach-session -t o2 ||  tmux new-session -A -s o2 -d 'less /Users/arun/personal/Personal/O2/O2_numbers.txt' \; split-window -h -d \; attach"



export LS_COLORS="di=34;1:fi=0:ln=92:pi=5:so=5:bd=5:cd=5:or=31;1;4:mi=0:ex=32;1:*.rpm=90:*.tar=00;31:*.tbz=00;31:*.tgz=00;31:*.rpm=00;31:*.deb=00;31:*.arj=00;31:*.taz=00;31:*.lzh=00;31:*.lzma=00;31:*.zip=00;31:*.zoo=00;31:*.z=00;31:*.Z=00;31:*.gz=00;31:*.bz2=00;31:*.tb2=00;31:*.tz2=00;31:*.tbz2=00;31:*.xz=00;31:*.avi=00;93:*.AVI=00;93:*.bmp=00;95:*.BMP=00;95:*.fli=00;95:*.gif=00;95:*.GIF=00;95:*.jpg=00;95:*.jpeg=00;95:*.JPG=00;95:*.JPEG=00;95:*.mng=00;93:*.mov=00;93:*.MOV=00;93:*.mpg=00;93:*.MPG=00;93:*.pcx=00;95:*.pbm=00;95:*.pgm=00;95:*.png=00;95:*.PNG=00;95:*.ppm=00;95:*.tga=00;95:*.tif=00;95:*.xbm=00;95:*.xpm=00;95:*.dl=00;95:*.gl=00;95:*.wmv=00;93:*.WMV=00;93:*.aiff=00;93:*.au=00;93:*.mid=00;93:*.mp3=00;93:*.MP3=00;93:*.ogg=00;93:*.OGG=00;93:*.voc=00;93:*.WAV=00;93:*.wav=00;93:*.chm=38;5;141:*.djvu=38;5;141:*.pdf=38;5;141:*.epub=38;5;141:*.PDF=38;5;141:*.ps=38;5;141:*.eps=38;5;141:*.docm=38;5;222;4:*.doc=38;5;222:*.docx=38;5;222:*.xls=38;5;222:*.xlsx=38;5;222:*.tex=38;5;222"
