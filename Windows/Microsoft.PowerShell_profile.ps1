function prompt  
{
    $ESC = [char]27
    "$ESC[31mWin$ESC[0m: $($executionContext.SessionState.Path.CurrentLocation)$(' -$' * ($nestedPromptLevel + 1)) "  
}

Set-Location C:\Users\archandr\Desktop

Import-Module PSColor

Set-PSReadLineOption -EditMode Emacs
Set-PSReadlineOption -BellStyle None
Set-PSReadlineKeyHandler -Key Ctrl+LeftArrow -Function ShellBackwardWord
Set-PSReadlineKeyHandler -Key Ctrl+RightArrow -Function ShellForwardWord
Set-PSReadLineKeyHandler -Key 'Escape,p' -Function HistorySearchBackward


Set-Alias -Name o -Value Invoke-Expression
Set-Alias -Name d -Value Set-Location
Set-Alias -Name l -Value Get-ChildItem
Set-Alias -Name k -Value Get-ChildItem
Set-Alias -Name � -Value Get-ChildItem
Set-Alias -Name ks -Value Get-ChildItem
Set-Alias -Name c -Value Clear-Host
Set-Alias -Name p -Value Get-Location
Set-Alias -Name le -Value Get-Content
Set-Alias -Name less -Value Get-Content
Set-Alias -Name mk -Value mkdir
Set-Alias -Name mo -Value more
Set-Alias -Name wh -Value Get-Command


Set-Alias -Name cr -Value C:\Program` Files\Google\Chrome\Application\chrome
Set-Alias -Name th -Value C:\Program` Files\Mozilla` Thunderbird\thunderbird

Function gww {Set-Location -Path C:\Users\archandr\Desktop\Work}
Function ga {Set-Location -Path C:\Users\archandr\Desktop\Arun}

Function cd2 {Set-Location -Path ..\..}
Function cd3 {Set-Location -Path ..\..\..}
Function cd4 {Set-Location -Path ..\..\..\..}

Function nt {explorer.exe .}

Function orw {ssh orw-archandr-vm }
Function g1 {ssh ones-grid-01 }
Function g2 {ssh ones-grid-02 }
Function g3 {ssh ones-grid-03 }
Function g4 {ssh ones-grid-04 }
Function g5 {ssh ones-grid-05 }
Function g6 {ssh ones-grid-06 }

Function emacs {C:\Program` Files\Emacs\x86_64\bin\runemacs.exe --geometry 95x50 $args }
Function t {C:\Program` Files\Emacs\x86_64\bin\runemacs.exe --geometry 95x50 C:\Users\archandr\Desktop\Work\Siemens\passwords.txt }


