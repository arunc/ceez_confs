1) Install emacs in Windows and make it work.
Note that the command to run emacs in windows is not "emacs", rather "runemacs".
"emacs" is some command linev version

2) Then always install VSCode.

3) Install the latest Windows Terminal

4) Instal 7.+ Windows powershell:
Install 7.2 or latest from Microsoft repository (default is 5.1, not good enough)
Use the profile  Microsoft.PowerShell_profile.ps1
This has to be copied to $PROFILE location.
At the time of writing this readMe, the location was C:\Users\archandr\Documents\PowerShell\Microsoft.PowerShell_profile.ps1
Do an echo $PROFILE to see the location


5) Set up WSL2
We may need to restart wsl several times,
wsl --shutdown

VsCode is automatically setup to work with WSL2.
Just run "code ." in the respective directory.
This will fire up the VS in Windows, but connected to WSL.
In the very first invocation, it will download and install the necessary components.


6) In the Windows-Terminal, use Ctrl-Shift-P to bring up the command palette
and edit the settings.json (User settings, not the default settings)
The current one we are using is saved as Windows-Terminal-settings.json

7) Get rid of Cortana, search bar and all other distractions.
   this changes with release, hence google first


8) VPN issues in Cisco MyConnect with WSL2,
First see if Microsoft/Cisco already fixed this 

    Something that worked is 
    a) change /etc/resolv.conf 
       -> use nslookup in powershell for the server
    =========================================================
    Win: C:\Users\archandr\Desktop\Arun\scripts -$ nslookup
    Default Server:  anycast-na.wv.mentorg.com
    Address:  147.34.2.16
    =========================================================
    Use that in the resolv.conf  # the current copy available in this directory.
    Edit the wsl.conf as mentioned there # current copy kept in this directory.
    
    b) Edit the "Interface Metric" network adapter settings for the 
    "Network and Internet Settings" -> "Change adapter options" -> "Cisco Anyconnect" -> "Properties" -> 
                For IPV4 -> Properties -> Advanced -> Interface Metric # Change the value to 6000 // originally was 1 for me
                For IPV6 -> Properties -> Advanced -> Interface Metric # Change the value to 6000 // originally was 1 for me
     
    c) see if really works, ping www.google.com


