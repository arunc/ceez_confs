1. For centos-custom startup script and use the xfce4-terminal drop down feature.
    -> gnome-session-properties
     -> Application Autostart
      -> Add an entry "centos-custom"
         Command: /usr/bin/centos-custom-startup.sh

2. Map capslock to AltGr for german keyboards
   Part of the centos-custom-startup.sh

3. Map F12 to drop-down terminal
  -> gnome-control-center keyboard
    -> Application Shortcuts
    -> Add new application: xfce4-terminal --drop-down
      and map it to F12
      The command is by default in toggling mode
    -> To remove F11, create another shortcut xfce4-terminal --drop-down and map it to F11 as before
       So far, this is the only working option
      
    -> Shortcuts::Navigation::Hide all normal windows = set this to Super+D


4. Map F1, F2, F3 etc to xfce4-terminal tabs and remove F11 ( F11 is fullscreen, this is not smooth in Gnome3 )
  -> Enable shortcut accelerator modifications
     -> xfce4-settings-manager
       -> Appearance -> Settings -> Enable Editable Accelerators
     ( http://docs.xfce.org/xfce/xfce4-settings/appearance#menu_and_buttons )
  -> enable menu in xfce4-terminal
  -> Create as many tabs as needed
  -> Press F1 in the tabs section in the Menu
  -> Record these shortcuts

  http://unix.stackexchange.com/questions/166427/is-there-a-shortcut-missing-to-move-tab-in-xfce4-terminal-app
  Alternately for terminal settings copy the terminal directory to ~/.config/xfce4/terminal
     -> this directory has .scm which to take care of keyboard shortcuts directly

5. Tint2
  -> mkdir ~/.config/tint2
  -> cp tint2rc  ~/.config/tint2/

