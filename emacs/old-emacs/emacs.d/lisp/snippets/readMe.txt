Arun: How to make snippets.
** Put snippets on a per mode basis
How to define snippets : 
http://cupfullofcode.com/blog/2013/02/26/snippet-expansion-with-yasnippet/index.html
https://joaotavora.github.io/yasnippet/snippet-development.html

Example Snippet::
***********************************************************
-*- mode: snippet -*-
# name: vector
# key: vec
# --
std::vector<$1>  $0

***********************************************************
Explanation:
$1 is the first input item. And on the first press of the tab 
it will go to where $0 is.
i.e., if we press vec-> TAB
      cursor will go to $1 position. Type what we want here.
      Then TAB
      cursor will go to $0 position. 
      In this eg, 2 spaces after the ">" exactly.

If we don't put $0 the cursor will go to the next line.

std::vector<${1:type}>  $0
The "type" will appear as a string message as an additional help. 
If nothing entered this "type" will be the default entry

