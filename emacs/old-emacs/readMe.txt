1)
Copy all these as hidden files in the home directory

cp emacs ~/.emacs
cp emacs_nw ~/.emacs_nw

cp -rf emacs.d ~/emacs.d
mv ~/emacs.d ~/.emacs.d

2)
Byte compile all the files in ~/.emacs.d/lisp
emacs -Q --batch -f batch-byte-compile ~/.emacs.d/lisp/*.el

Open emacs and byte-compile dash and flycheck seprately.
M-x byte-compile-file ~/.emacs.d/lisp/flycheck/dash.el 
M-x byte-compile-file ~/.emacs.d/lisp/flycheck/flycheck.el 



