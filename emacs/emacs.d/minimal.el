;; to get rid of the menu.
(menu-bar-mode -1) 
(tool-bar-mode -1)
(scroll-bar-mode -1)

(global-set-key [delete] `delete-char)
(global-set-key [insert] `string-insert-rectangle)
(global-set-key (kbd "\C-xri")    `string-insert-rectangle)
(global-set-key [home] `beginning-of-line)
(global-set-key [end] `end-of-line)
 
(setq completion-ignore-case t)
(global-set-key (kbd "C-z") nil)
(global-set-key (kbd "C-f") 'golden-ratio-scroll-screen-up)
(global-set-key (kbd "C-b") 'golden-ratio-scroll-screen-down)
(global-set-key (kbd "C-z C-z") 'suspend-emacs)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-v") 'dabbrev-expand) ;; expand the string
(global-set-key (kbd "C-s") 'isearch-forward-regexp) ;; regexp instead of regular


(global-linum-mode t)
(setq linum-format "%4d \u2502 ") ;; the line after linenumber
(set-face-background 'fringe "gray20")

(global-set-key (kbd "C-x C-l") 'goto-line)
(global-set-key "\C-o"  'find-file-at-point)

;;------------------------------------------------------------------------------
;; Note: in the german keyboad this will look like C-S-minus and C-S-plus
;; thats the only rational for this key combination
(global-set-key (kbd "C-_") 'undo)

;; this is for smooth scrolling
(setq scroll-preserve-screen-position t)
(setq scroll-error-top-bottom t)
(setq scroll-step 1 scroll-conservatively 10000)
(setq next-screen-context-lines 4)

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
(setq create-lockfiles nil)

(global-set-key (kbd "C-q") "@")
(global-set-key (kbd "C-^") "~")
(global-set-key (kbd "C-1") "|")
(global-set-key (kbd "C-5") "[")
(global-set-key (kbd "C-6") "]")
(global-set-key (kbd "C-7") "{")
(global-set-key (kbd "C-8") "{")
(global-set-key (kbd "C-9") "}")
(global-set-key (kbd "C-0") "}")
(global-set-key (kbd "C-ß") "\\")

(setq-default frame-title-format '("%b"))

 (custom-set-faces
  '(default ((t (:family "Ubuntu Mono" :foundry "DAMA" :slant normal :weight normal :height 120 :width normal))))
  )
