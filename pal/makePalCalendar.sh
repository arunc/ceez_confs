#!/usr/bin/env bash

YEAR=2020
OUT=x.xlsx
CSV=x.csv
URL=https://www.generalblue.com/calendar/downloads/germany-holidays-${YEAR}-list-classic-en-de.xlsx
PAL=bayern.pal

\rm -f $CSV
\rm -f $OUT
\rm -f $PAL

wget -O $OUT $URL
ssconvert $OUT $CSV

touch $PAL
echo "BY Germany" >> $PAL
cat $CSV | grep -v DATE | grep -v Holidays | sed "s/${YEAR}\//0000/g" | sed 's/\///g'  | sed 's/\s+//g' | awk -F, '{print $1, $3}' >> $PAL


